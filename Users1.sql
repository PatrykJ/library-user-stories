CREATE DATABASE IF NOT EXISTS Users1;

CREATE TABLE IF NOT EXISTS students	(
	id				INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    UserName		VARCHAR(30) NOT NULL,
    pass			VARCHAR(10) NOT NULL,
    firstName		VARCHAR(15) NOT NULL,
    lastName		VARCHAR(15) NOT NULL,
    MobileNum		INTEGER NOT NULL);
    
CREATE TABLE IF NOT EXISTS admin		(
	id				INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
	UserName		VARCHAR(30) NOT NULL,
    NickName		VARCHAR(30) NOT NULL,
    pass			VARCHAR(10) NOT NULL,
    firstName		VARCHAR(15) NOT NULL,
    lastName		VARCHAR(15) NOT NULL,
    MobileNum		INTEGER NOT NULL);
    
INSERT INTO students VALUES (null, 'oskar', 'klosowski69', 'Oskar', 'Klosowski', 070088077);
INSERT INTO students VALUES (null, 'homer21', 'simpson34', 'Homer', 'Simpson', 0831245858);
INSERT INTO students VALUES (null, 'long21', 'strong18', 'Anna', 'Strong', 0875221542);
INSERT INTO admin VALUES(null, 'adminX123', 'TheLegend27', 'IamAdmin23', 'Owner', 'OfTheSystem', 0878221312);    